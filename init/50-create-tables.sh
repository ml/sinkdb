#!/usr/bin/env bash

# pre-create rule tables for existing twitter rules

echo "getting rules from Twitter API"
mapfile -t rules < <(
    curl -X GET "https://api.twitter.com/2/tweets/search/stream/rules" \
        --silent \
        --header "Authorization: Bearer $TWITTER_BEARER_TOKEN" |
        jq -r '.data | .[] | .id + " " + .value '
)

schefix="rules.rule_"

for rule in "${rules[@]}"; do

    # separate id and value from rule string
    rule_id="${rule%% *}"
    rule_value="${rule#"$rule_id" }"

    # assemble table name
    table_name="$schefix$rule_id"

    echo "creating table $rule_id with $rule_value"
    psql -v ON_ERROR_STOP=1 --dbname "$DATABASE_NAME" -U postgres \
        -c "
        CREATE TABLE IF NOT EXISTS $table_name (
            geohash varchar(4),
            PRIMARY KEY (geohash),
            id hll
        );
        COMMENT ON TABLE $table_name IS '$rule_value';
        "
done

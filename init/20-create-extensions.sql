/* Create extensions schema */

CREATE SCHEMA extensions;

/* Create PostGIS extension
 * PostGIS is a spatial database extender for PostgreSQL
 * object-relational database. It adds support for geographic
 * objects allowing location queries to be run in SQL.
 * https://postgis.net/
 */
CREATE EXTENSION IF NOT EXISTS postgis SCHEMA extensions;

/* Create Hyperloglog extension
 * PostgreSQL extension adding HyperLogLog data structures
 * as a native data type
 * https://github.com/citusdata/postgresql-hll
 */
CREATE EXTENSION IF NOT EXISTS hll SCHEMA extensions;
ALTER DATABASE :DBNAME SET search_path TO "$user", extensions;

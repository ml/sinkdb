# sinkDB

Containerized database environment for the [vgisink] project. Built on top of
Postgres, PostGIS and [Citus], it comes with full **[HyperLogLog]** support.

`cp .env.example .env` and adjust environment variables to initialize and access
the database and Twitter API.

    SINKDB_NAME
    SINKDB_USER
    SINKDB_PASS
    TWITTER_BEARER_TOKEN

Optional setting to include in external container networks:

    EXTERNAL_NETWORK=true

The init script creates a table for each *rule* defined in the [filtered stream]
of the Twitter developer account.

[vgisink]: https://gitlab.vgiscience.de/ml/vgisink
[Citus]: https://github.com/citusdata/citus
[HyperLogLog]: https://github.com/citusdata/postgresql-hll
[filtered stream]: https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/

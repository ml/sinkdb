FROM postgres:13

# https://manpages.debian.org/stretch/debconf-doc/debconf.7.en.html#Frontends
ARG DEBIAN_FRONTEND=noninteractive
ARG DEBCONF_NOWARNINGS=yes

# Postgis
# https://github.com/postgis/docker-postgis
RUN set -ex; \
    apt-get update; \
    apt-get install --assume-yes --no-install-recommends \
        postgresql-13-postgis-3 \
        postgresql-13-postgis-3-scripts \
        postgis \
        ca-certificates \
    ; \
    rm -rf /var/lib/apt/lists/*

# Citus Plugins
# due to general paranoia, do not `curl | bash` but use local script copy
# get it from https://github.com/citusdata/docker
COPY get-citusdata-packages.sh .
RUN bash get-citusdata-packages.sh && rm get-citusdata-packages.sh
RUN apt-get install --assume-yes --no-install-recommends \
        postgresql-13-citus-10.0 \
        postgresql-13-hll \
        postgresql-13-topn \
        jq \
    ; \
    rm -rf /var/lib/apt/lists/*

ADD init /init
COPY init.sh /docker-entrypoint-initdb.d/init.sh
